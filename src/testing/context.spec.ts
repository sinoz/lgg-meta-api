import test from "tape-promise/tape.js";
import { withContext } from "./context.js";

if (!process.env.CI) test("with-context", t => withContext(async context => {
    t.pass();
}));
