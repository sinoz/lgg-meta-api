import { spawnSync } from "child_process";
import { program } from "commander";
import glob from "glob";

program.
    command("run-all <pattern>").
    action(action);

async function action(pattern: string) {
    const files = glob.sync(pattern);

    for (const file of files) {
        const result = spawnSync(process.argv0, [file], { stdio: "inherit" });
        if (result.status) process.exitCode = 1;
    }
}
