import * as metaApi from "@latency.gg/lgg-meta-oas";
import { Context } from "./context.js";

export type Server = metaApi.Server;

export function createServer(
    context: Context,
) {
    const server = new metaApi.Server({
        baseUrl: context.config.endpoint,
    });

    server.registerLivenessOperation(() => {
        if (context.signals.liveness.aborted) {
            return {
                status: 503,
                parameters: {},
            };
        }

        return {
            status: 204,
            parameters: {},
        };
    });

    server.registerReadinessOperation(() => {
        if (context.signals.readiness.aborted) {
            return {
                status: 503,
                parameters: {},
            };
        }

        return {
            status: 204,
            parameters: {},
        };
    });

    server.registerMetricsOperation(() => {
        return {
            status: 200,
            parameters: {},
            async value() {
                const value = await context.config.promRegistry.metrics();
                return value;
            },
        } as const;
    });

    return server;
}
