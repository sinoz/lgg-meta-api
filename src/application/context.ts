import { Config } from "./config.js";
import { createSignals, Signals } from "./signals.js";

export interface Context {
    config: Config;
    signals: Signals
}

export function createContext(
    config: Config,
): Context {
    const signals = createSignals(config);

    return {
        config,
        signals,
    };
}
