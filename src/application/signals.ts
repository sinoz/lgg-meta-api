import { Config } from "./config.js";

export interface Signals {
    readiness: AbortSignal,
    liveness: AbortSignal,
}

export function createSignals(config: Config): Signals {
    const signals = {
        readiness: config.readinessController.signal,
        liveness: config.livenessController.signal,
    };
    return signals;
}
